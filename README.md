LED控制
===============

LED控制 在是一个简单的ARM平台上控制LED设备的小程序

 + 基于LINUX系统的ARM平台
 + 使用的是GO语言开发
 + 请勿在自己电脑测试
 + 操作前请熟悉自己的设备硬件以免造成设备损坏


> LED控制 需要自己使用GO语言编译生成

## 目录结构

初始的目录结构如下：

~~~
/        （根目录目录）
├─main.go           程序源码
~~~


## 版权信息

LED控制 遵循MIT开源协议发布，并提供免费使用，使用本程序不承担任何法律及相关责任，使用本程序即为同意所描述责任。

版权所有Copyright © 2016 by LED控制 (http://0-w.cc)

All rights reserved。
