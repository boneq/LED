package main

import (
	"fmt"
	"log"
	"os"
	"time"
)

func main() {
	gpio := "/sys/class/gpio/export"            //系统的GPIO调用文件，为了暴露GPIO操作文件
	out := "/sys/class/gpio/gpio21/direction"   //具体的GPIO21操作输入输出文件，可以修改gpio21来操作对应GPIO口
	value := "/sys/class/gpio/gpio21/value"     //输出为1否则为0
	input("21", gpio)
	fmt.Printf("打开21GPIO口\n")
	time.Sleep(1 * time.Second)
	input("out", out)
	fmt.Printf("更改模式为输出\n")
	time.Sleep(1 * time.Second)
	for i := 1; i <= 10; i++ {
		input("1", value)
		fmt.Printf("输出高电频开灯%d次\n", i)
		time.Sleep(1 * time.Second)
		input("0", value)
		fmt.Printf("输出低电频关灯%d次\n", i)
		time.Sleep(1 * time.Second)
	}
	fmt.Printf("程序结束!!\n")
}
func input(message string, files string) {
	file, err := os.OpenFile(files, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		log.Fatal(err)
	}
	file.WriteString(message)
	file.Close()
}
